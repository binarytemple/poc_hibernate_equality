Introduction
============

This is a small project to research the following:

* Hibernate Set behavior specifically regarding the behavior of equals() and hashcode().
* Auto-generation of timestamps via the @PrePersist and @PreUpdate annotations.

Read more at 
[ https://community.jboss.org/wiki/EqualsAndHashCode ] 

Pre-requisites
--------------
Maven

The results
-----------
I have redefined object equality/hashcode on the basis of the sourceId.

# Equals/Hashcode Conclusion

JPA does not add duplicate Person entities to the staff collection of Company.
I create all Person entities with the same sourceId (1).
Repeatedly inserts to the staff collection are ignored.
Only the first added is persisted.

On the second run, I call remove on the staff set, passing in the new entity, then call add with the new entity. Only the most recent copy of the entity is added to the staff collection.

# Auto-generation of timestamps Conclusion

I generate the timestamp and convert to a Long data type, before persisting or updating the entity.