import ie.hunt.Comment
import javax.persistence.{EntityTransaction, EntityManager, Persistence}
import org.apache.log4j.Logger

class TimestampExampleMain

object TimestampExampleMain extends App {

  val RECORDS_TO_INSERT: Int = 3
  val emf = Persistence.createEntityManagerFactory("timestamp_example")
  var em: EntityManager = emf.createEntityManager()
  val logger = Logger.getLogger(classOf[TimestampExampleMain])

  val t2: EntityTransaction = em.getTransaction
  t2.begin()
  val c = new Comment
  c.setComment("blah")
  logger.info("Pre-commit:" + c)
  em.persist(c)
  logger.info("Post-first-commit:" + c)
  c.setComment("blah 2")
  em.merge(c)
  logger.info("Post-merge:" + c)
  t2.commit()
  logger.info("Post-transaction:" + c)

}