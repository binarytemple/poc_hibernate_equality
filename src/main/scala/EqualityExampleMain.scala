import ie.hunt.{Person, Company}
import java.util
import javax.persistence.{EntityTransaction, EntityManager, Persistence}


object EqualityExampleMain extends App {

  val RECORDS_TO_INSERT: Int = 3
  val emf = Persistence.createEntityManagerFactory("timestamp_example")

  var em: EntityManager = emf.createEntityManager()
  def clearDatabase() {

    def tryDo(op: => Int) = {
      val em2: EntityManager = emf.createEntityManager()
    val t: EntityTransaction = em2.getTransaction
    try{
      t.begin()
      op
      t.commit()
    }catch {
      case e:Throwable => println(e.toString)
    }
    finally {
      if (t.isActive) {
        t.rollback()
      }
      em2.close()
    }

  }
    tryDo {em.createNativeQuery("DELETE FROM company_person").executeUpdate() }
    tryDo {em.createNativeQuery("DELETE FROM person").executeUpdate()}
    tryDo {em.createNativeQuery("DELETE FROM company").executeUpdate()}
  }
  clearDatabase()

  def insertData(companyName:String)(closure: (Person, util.Set[Person]) => Boolean): util.List[Company] = {
    val t2: EntityTransaction = em.getTransaction
    t2.begin()
    val c = new Company()
    c.setTitle(companyName)
    em.persist(c)
    //persisted the company
    val persons = Range(1, RECORDS_TO_INSERT).map(j => {
      val p = new Person
      p.setSourceId("1")
      p.setTitle("dave")
      p.setGreeting("greeting" + j)
      p
    })
    persons.foreach(p => em.persist(p))
    //persisted the persons
    val staff: util.Set[Person] = c.getStaff
    persons.foreach(p => closure(p, staff))
    em.merge(c)
    //added all persons (only one because they all have the same sourceId) to company and persisted the company
    t2.commit()
    val res: util.List[Company] = em.createQuery("select distinct c from Company c inner join fetch c.staff", classOf[Company]).getResultList
    res
  }

  def justadd: (Person, util.Set[Person]) => Boolean = {
    (p, s) => {
      s.add(p)
    }
  }

  def removeThenAdd: (Person, util.Set[Person]) => Boolean = {
    (p, s) => {
      s.remove(p)
      s.add(p)
    }
  }

  import scala.collection.JavaConversions._

  insertData("company_justadd"){
    justadd
  }

  val all_companies = insertData("company_remove_then_add"){
    removeThenAdd
  }
  for {c <- all_companies
       p <- c.getStaff} {
    println(c)
    println(p)
  }
}