package ie.hunt;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

/**
 * User: bryan
 * Date: 9/17/12
 * Time: 1:58 PM
 */
@Entity
@Table(name = "person")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@BatchSize(size = 1000)
public class Person implements Serializable {

    private Long id;
    private String title;
    private String greeting;
    private String sourceId;

    @Basic
    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false, precision = 15, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", greeting='" + greeting + '\'' +
                ", sourceId='" + sourceId + '\'' +
                '}';
    }

    @Basic
    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;
        //sourceId equality is sufficient.
        if(sourceId.equals(person.getSourceId())) return true;

        if (!id.equals(person.id)) return false;
        if (title != null ? !title.equals(person.title) : person.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        if(sourceId == null) return 0;
        int result = sourceId.hashCode();
        result = 31 * result;
        return result;
    }
}