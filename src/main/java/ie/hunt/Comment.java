package ie.hunt;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * User: bryan
 * Date: 9/17/12
 * Time: 1:58 PM
 */
@Entity
@Table(name = "comment")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@BatchSize(size = 1000)
public class Comment implements Serializable {

    private Long id;
    private String comment;
    private Long lastUpdated;

    public void setId(Long id) {
        this.id = id;
    }



    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setLastUpdated(Long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @PrePersist
    void onPersist() {
        this.lastUpdated = new Date().getTime();
    }

    @PreUpdate
    void onUpdate() {
        this.lastUpdated = new Date().getTime();
    }

    @Column(name="last_updated")
    public Long getLastUpdated() {
        return lastUpdated;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false, precision = 15, scale = 0)
    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", comment='" + comment + '\'' +
                ", lastUpdated=" + ((lastUpdated != null) ? lastUpdated: 0 + "") +
                '}';
    }
}